package nuc.edu.controller;



import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import nuc.edu.dao.UserMapper;
import nuc.edu.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class Controller {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/test1")
    public List<User> test1() {
        List<User> users = userMapper.selectList(null);

        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }


    @RequestMapping("/test2")
    public List<User> test2(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("username", name);
        return userMapper.selectByMap(map);
    }

    @RequestMapping("/save")
    public String save(String username, String password) {
        User user = new User(username,password);

        int insert = userMapper.insert(user);
        if (insert > 0) {
            return "添加成功";
        } else {
            return "添加失败";
        }
    }

    @RequestMapping("/update")
    public String update(Integer id, String username, String password) {
        User user = new User(id, username, password);

        int i = userMapper.updateById(user);
        if (i > 0) {
            return "修改成功， 影响行数为：" + i;
        } else {
            return "修改失败";
        }
    }


    @RequestMapping("/selectOne")
    public User selectOne(Integer id) {
        return userMapper.selectById(id);
    }


    @RequestMapping("/page")
    public List<User> page(Integer pageNumber, Integer pageSize) {
        PageHelper.startPage(pageNumber, pageSize);

        return userMapper.selectList(null);
    }






}
