package nuc.edu.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @RequestMapping(value = "/rs")
    public String rs(HttpServletRequest request, HttpSession session) {
        request.setAttribute("data", "request1");
        request.getSession().setAttribute("data1", "123");
        session.setAttribute("flag", "session1");

        return "rs";
    }
}
