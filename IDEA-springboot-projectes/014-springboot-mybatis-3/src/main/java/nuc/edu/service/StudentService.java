package nuc.edu.service;


import nuc.edu.model.Student;

public interface StudentService {

    Student queryStudentById(Integer id);

}
