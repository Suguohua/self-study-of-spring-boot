package nuc.edu.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MyController {

    @RequestMapping(value = "myservlet")
    public @ResponseBody String servlet() {
        return "Hello MyServlet";
    }
}
