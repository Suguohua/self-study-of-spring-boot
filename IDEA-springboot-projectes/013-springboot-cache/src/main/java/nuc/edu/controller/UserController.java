package nuc.edu.controller;

import nuc.edu.dao.UserMapper;
import nuc.edu.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/insert")
    @CachePut(value = "user" , key = "#id")
    public String test(Integer id, String username, String password) {
        User user = new User(id, username, password);

        int i = userMapper.insert(user);
        if (i > 0) {
            return "修改成功";
        } else {
            return "修改失败";
        }
    }

    @RequestMapping("/select")
    @Cacheable(value = "user", key = "#id")
    public User select(Integer id) {
        return userMapper.selectById(id);
    }

    @RequestMapping("/delete")
    @CacheEvict(value = "user", key = "#id")
    public String delete(Integer id) {
        int i = userMapper.deleteById(id);

        if (i > 0) {
            return "删除成功";
        }else {
            return "删除失败";
        }
    }





}
