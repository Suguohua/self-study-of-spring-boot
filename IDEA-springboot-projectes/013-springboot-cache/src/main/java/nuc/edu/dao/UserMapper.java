package nuc.edu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import nuc.edu.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {

}
