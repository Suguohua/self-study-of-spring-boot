package nuc.edu.controller;

import com.github.pagehelper.PageHelper;
import nuc.edu.dao.UserMapper;
import nuc.edu.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @RequestMapping(value = "/select")
    public List<User> select(Integer pageNum, Integer pageSize) {
        List<User> userList = userMapper.getUserList();
            for (User user : userList) {
            System.out.println(user);
        }

        PageHelper.startPage(pageNum, pageSize);
        return userMapper.getUserList();
    }



}
