package nuc.edu.service;

import nuc.edu.entity.Student;

public interface StudentService {

    Student queryStudentByID(Integer id);

    Integer queryAllStudent();

}
