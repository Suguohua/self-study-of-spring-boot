package nuc.edu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import nuc.edu.service.StudentService;
import org.springframework.stereotype.Component;

@Component
@Service(interfaceClass = StudentService.class, version = "1.0.0", timeout = 10000)
public class StudentServiceImpl implements StudentService {

    @Override
    public Integer queryAllStudentCount() {
        return 2111;
    }
}
