package nuc.edu.web;

import nuc.edu.model.User;
import nuc.edu.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    @Autowired
    @Qualifier("userservice")
    private UserService userService;

    @Autowired
    @Qualifier("userservice2")
    private UserService userService2;

    @RequestMapping(value = "/userDetail")
    public String userDetail(Model model, Integer id, String username) {
        User user = userService.queryUserById(id, username);
        User user1 = userService2.queryUserById(id, username);
        model.addAttribute("user", user);
        model.addAttribute("user2",user1);
        return "userDetail";
    }
}
