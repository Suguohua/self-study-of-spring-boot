package nuc.edu;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

@SpringBootTest
class ApplicationTests {

	@Test
	void contextLoads() {
		try {
			System.out.println(URLDecoder.decode("%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e9%a6%a8%e5%9b", "UTF-8"));
			System.out.println(URLDecoder.decode("%ad%e5%8d%97%e8%b7%af","UTF-8"));
			System.out.println(URLDecoder.decode("%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e8%a1%8c%e7%9f","UTF-8"));
			System.out.println(URLDecoder.decode("%a5%e8%a5","UTF-8"));
			System.out.println(URLDecoder.decode("%bf%e8%b7%af","UTF-8"));
			System.out.println(URLDecoder.decode("%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e4%b8%9c%e7%8e","UTF-8"));
			System.out.println(URLDecoder.decode("%af","UTF-8"));
			System.out.println(URLDecoder.decode("%E5%B1%B1%E8%A5%BF%E7%9C%81%E5%A4%AA%E5%8E%9F%E5%B8%82%E5%B0%96%E8%8D%89%E5%9D%AA%E5%8C%BAX256","UTF-8"));

			System.out.println(URLDecoder.decode("%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e9%a6%a8%e5%9b%ad%e5%8d%97%e8%b7%af%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e8%a1%8c%e7%9f%a5%e8%a5%bf%e8%b7%af%e5%b1%b1%e8%a5%bf%e7%9c%81%e5%a4%aa%e5%8e%9f%e5%b8%82%e5%b0%96%e8%8d%89%e5%9d%aa%e5%8c%ba%e4%b8%9c%e7%8e%af%E5%B1%B1%E8%A5%BF%E7%9C%81%E5%A4%AA%E5%8E%9F%E5%B8%82%E5%B0%96%E8%8D%89%E5%9D%AA%E5%8C%BAX256", "UTF-8"));


			System.out.println(URLDecoder.decode("%E5%81%A5%E5%BA%B7", "UTF-8"));
			System.out.println(URLDecoder.decode("%E5%90%A6", "UTF-8"));
			System.out.println(URLDecoder.decode("%E5%90%A6", "UTF-8"));
			System.out.println(URLDecoder.decode("%E5%B1%B1%E8%A5%BF%E7%9C%81", "UTF-8"));
			System.out.println(URLDecoder.decode("%E5%A4%AA%E5%8E%9F%E5%B8%82", "UTF-8"));
			System.out.println(URLDecoder.decode("%E5%B0%96%E8%8D%89%E5%9D%AA%E5%8C%BA", "UTF-8"));

			System.out.println(URLEncoder.encode("山西省"));
			System.out.println(URLEncoder.encode("大同市"));
			System.out.println(URLEncoder.encode("云冈区迎新街"));
			System.out.println(URLEncoder.encode("山西省大同市云冈区迎新街"));
			System.out.println(URLEncoder.encode("山西省大同市云冈区文化街"));
			System.out.println(URLEncoder.encode("山西省大同市云冈区福园小区"));
			System.out.println(URLEncoder.encode("山西省大同市云冈区平旺公园"));


		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
