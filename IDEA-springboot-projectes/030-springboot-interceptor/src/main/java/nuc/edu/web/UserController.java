package nuc.edu.web;

import nuc.edu.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/user")
public class UserController {

    // 登录
    @RequestMapping(value = "/login")
    public @ResponseBody Object login(HttpServletRequest request) {
        User user = new User(1001, "zhangsan");
        request.getSession().setAttribute("user", user);
        return "login Success";
    }

    // 登陆后可以访问
    @RequestMapping("/center")
    public @ResponseBody Object center() {
        return "See Center Message";
    }

    // 不登录也可以访问
    @RequestMapping("/out")
    public @ResponseBody Object out() {
        return "Out see anyTime";
    }

    // 如果用户在未登录的情况下访问了登录才可以访问的请求，则会跳转到此路径
    @RequestMapping("/error")
    public @ResponseBody Object error() {
        return "error";

    }


}
