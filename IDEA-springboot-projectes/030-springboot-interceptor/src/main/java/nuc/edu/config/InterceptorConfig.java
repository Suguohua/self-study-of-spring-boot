package nuc.edu.config;

import nuc.edu.interceptor.UserInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 定义此类为配置文件(即相当于之前的xml配置文件)
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    // mvc:interceptors
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 要排除的路径,排除的路径说明不需要登录
        String[] excludePath = {
                "/user/out",
                "/user/error",
                "/user/login"
        };

        // 要拦截user下的所有访问请求,必须登录后才可访问
        String[] addPath = {
                "/user/center"
        };
        System.out.println("拦截器配置文件============");
        registry.addInterceptor(new UserInterceptor()).addPathPatterns(addPath).excludePathPatterns(excludePath);
    }
}
