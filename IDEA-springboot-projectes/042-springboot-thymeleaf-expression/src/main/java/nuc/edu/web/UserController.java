package nuc.edu.web;

import nuc.edu.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @RequestMapping(value = "/user/detail")
    public ModelAndView Detail() {
        ModelAndView mav = new ModelAndView();
        User user = new User();
        user.setId(1001);
        user.setAge(22);
        user.setName("zhangsan");
        mav.setViewName("userDetail");
        mav.addObject("user", user);

        return mav;
    }

    @RequestMapping(value = "/user/detail1")
    public ModelAndView Detail1() {
        ModelAndView mav = new ModelAndView();
        User user = new User();
        user.setId(1002);
        user.setAge(33);
        user.setName("lisi");
        mav.setViewName("userDetail");
        mav.addObject("user", user);

        return mav;
    }

    @RequestMapping(value = "/url")
    public String Url(Model model) {
        model.addAttribute("id", 1001);
        model.addAttribute("age", 22);
        model.addAttribute("username", "zhaoliu");

        return "url";
    }

    @RequestMapping(value = "/test")
    public @ResponseBody String test(String username) {
        return "请求路径/test，参数是" + username;
    }

    @RequestMapping(value = "/test1")
    public @ResponseBody String test1(Integer id, Integer age, String username) {
        return "请求路径/test1，id是" + id + ",age是：" + age + "，username是：" + username;
    }

    @RequestMapping(value = "/test2/{username}")
    public @ResponseBody String test2(@PathVariable("username") String username) {
        return "请求路径/test2，username=" + username;
    }

    @RequestMapping(value = "/test3/{username}/{age}")
    public @ResponseBody String test3(@PathVariable("username") String username, @PathVariable("age") Integer age) {
        return "请求路径/test3，username=" + username + ",age=" + age;
    }

    @RequestMapping(value = "/property")
    public String property() {
        return "property";
    }

}
