package nuc.edu;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubboConfig;
import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
/*
	找不到或无法加载主类，解决：
		将接口工程 打包方式设置为jar clean install package

	将提供者、消费者clean install
 */
public class Application1 {

	public static void main(String[] args) {
		SpringApplication.run(Application1.class, args);
	}

}
