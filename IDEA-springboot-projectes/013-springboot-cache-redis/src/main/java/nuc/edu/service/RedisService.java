package nuc.edu.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    @Resource
    @Qualifier("redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;

    /*
        此处要注意 在连接Linux的redis时， 要指定使用redis文件夹下的redis.conf配置文件
            或者修改其默认配置文件：  protected-mode yes改为protected-mode no（在没有密码的情况下，关闭保护模式）
                                   注释掉bind 127.0.0.1（取消绑定本地地址）
     */


    public void set(String key, Object value) {
        /*
            将key和value的值进行序列化， 即将unicode编码 序列化为我们认识的形式
         */
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Object>(Object.class));
        ValueOperations<String, Object> vo = redisTemplate.opsForValue();
        vo.set(key, value);
    }


    /**
     * 设置有生存时间得key-value 即缓存效果
     * @param key 键
     * @param value 值
     * @param time 存活时间
     * @param t 存活时间的单位
     */
    public void set(String key, Object value, Long time, TimeUnit t) {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<Object>(Object.class));
        ValueOperations<String, Object> vo = redisTemplate.opsForValue();
        vo.set(key, value, time, t);
    }



    public Object get(String key) {
        ValueOperations<String, Object> vo = redisTemplate.opsForValue();
        return vo.get(key);
    }


}
