package nuc.edu.controller;

import nuc.edu.entity.User;
import nuc.edu.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class RedisController {

    @Autowired
    private RedisService service;

    @RequestMapping("/save")
    public String saveUser(Integer id, String username, String password) {
        User user = new User(id, username, password);
        service.set(id.toString(), user);
        return "成功";
    }

    @RequestMapping("/cacheSave")
    public String CacheSaveUser(Integer id, String username, String password) {
        User user = new User(id, username, password);
        service.set(id.toString(), user, 30L, TimeUnit.SECONDS);
        return "成功";
    }


    @RequestMapping("/getUserById")
    public Object getUserById(Integer id) {
        return service.get(id.toString());
    }


}
