package nuc.edu.web;

import nuc.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/put")
    public @ResponseBody Object put(String key, String value) {
        studentService.put(key, value);
        return "值已经成功放入";
    }

    @RequestMapping(value = "/get")
    public @ResponseBody String get() {
        return "数据count为" + studentService.get("name");
    }

}
