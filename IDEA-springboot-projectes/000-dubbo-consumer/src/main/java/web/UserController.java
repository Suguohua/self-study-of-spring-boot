package web;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import service.UserService;

import javax.annotation.Resource;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/userDetail")
    public String userDetail(Model model, Integer id) {

        // 根据用户标识获取用户
        User user = userService.queryUserById(id);
        //获取用户的总人数
        int i = userService.queryAllUserCount();

        model.addAttribute("user", user);
        model.addAttribute("count", i);
        return "userDetail";
    }
}
