package nuc.edu.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import nuc.edu.dao.StudentMapper;
import nuc.edu.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
// interfaceClass 或 interfaceName 均可  消费者和提供者要统一
@Service(interfaceName = "nuc.edu.service.StudentService", version = "1.0.0", timeout = 10000)
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Override
    public Student queryStudentByID(Integer id) {
        return studentMapper.selectByPrimaryKey(id);
    }


    @Override
    public Integer queryAllStudent() {
        // 首先从redis缓存中查询，如果有直接使用；没有，则去数据库中查询并存放到redis缓存中
        // 提升系统性能
        Integer allcount = (Integer) redisTemplate.opsForValue().get("count");
        redisTemplate.setKeySerializer(new StringRedisSerializer());

        if (null == allcount) {
            allcount = studentMapper.selectAllStudentCount();

            redisTemplate.opsForValue().set("count", allcount,30 ,TimeUnit.SECONDS);
        }


        return allcount;
    }
}
