package nuc.edu.web;

import lombok.extern.slf4j.Slf4j;
import nuc.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping("/student/count")
    public @ResponseBody String student() {
        log.info("查询当前学生总人数");
        Integer count = studentService.queryStudentCount();
        return "学生的总人数为：" + count;
    }

}
