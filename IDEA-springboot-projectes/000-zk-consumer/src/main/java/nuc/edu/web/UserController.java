package nuc.edu.web;

import nuc.edu.model.User;
import nuc.edu.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping(value = "/user")
    public String queryById(Model model, Integer id, String username) {
        User user = userService.queryUserById(id, username);
        model.addAttribute("user", user);
        return "view/userDetail";
    }

}
