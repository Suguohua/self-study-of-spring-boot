package nuc.edu.test;


import java.util.Scanner;

public class Code01 {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        int[] test = new int[n];
        for (int i = 1; i <= n; i++) {
            test[i-1] = i;
        }

        int sum = 0;
        int happy = 0;
        int count = 0;

        for (int i = 0; i < test.length; i++) {
            for (int j = 1; j<=test[i]; j++) {
                if ((test[i] % j) == 0) {
                    sum += j;
                }
            }
            if ((sum / test[i]) < 2) {
                count += test[i];
            }
            sum = 0;
        }
        System.out.println(count);
    }
}
