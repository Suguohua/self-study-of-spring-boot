package nuc.edu.controller;

import nuc.edu.config.Book;
import nuc.edu.config.School;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Scanner;

@Controller
public class IndexController {

    @Value("${school.name}")
    private String schoolName;
    @Value("${school.website}")
    private String website;

    @Autowired
    private School school;

    @Autowired
    private Book book;

    @RequestMapping(value = "/say")
    public @ResponseBody String say() {
        StringBuilder sb = new StringBuilder();
        sb.append(schoolName + website).append("  ").append(school.getName() + school.getWebsite()).append("  ").append(book.getName()).append(book.getWebsite());
        return sb.toString();
    }
}
