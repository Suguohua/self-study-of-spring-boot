package nuc.edu.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    @RequestMapping(value = "/user/detail")
    public String userDetail(Model model) {
        model.addAttribute("id", 1001);
        model.addAttribute("data", "SpringBoot框架集成Thymeleaf模板引擎");
        return "message";
    }
}
