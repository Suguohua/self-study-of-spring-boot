package nuc.edu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import nuc.edu.enetiy.User;
import org.springframework.stereotype.Repository;

@Repository
public interface TestMapper extends BaseMapper<User> {

}
