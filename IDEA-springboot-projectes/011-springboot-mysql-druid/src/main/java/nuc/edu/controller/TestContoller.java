package nuc.edu.controller;


import nuc.edu.dao.TestMapper;
import nuc.edu.enetiy.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestContoller {

    @Autowired
    private TestMapper testMapper;

    @RequestMapping("/test")
    public User test(Integer id, HttpServletRequest request) {
        User user = testMapper.selectById(id);
        request.setAttribute(id.toString(), user);
        return user;
    }


}
