package nuc.edu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


@Configuration //配置类
@EnableSwagger2// 开启Swagger2的自动配置
public class SwaggerConfig {

    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("A");
    }

    @Bean
    public Docket docket2() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("B");
    }
    @Bean
    public Docket docket3() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("C");
    }


    // 配置了Swagger的Docket的bean实例
    @Bean
    public Docket getDocket(Environment environment) {

        // 设置要显示的Swagger环境  如下为指定dev和test环境可以使用Swagger
        Profiles profiles = Profiles.of("dev","test");
        // 通过environment.acceptsProfiles判断当前环境是否处在自己设定的环境当中
        boolean b = environment.acceptsProfiles(profiles);
        // 将这个返回的boolean 传给enable，就可以做到在指定环境下使用Swagger

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("wt")
                .apiInfo(apiInfo())
                // enable:配置是否启用Swagger，如果是false，在浏览器将无法访问
                .enable(b)
                .select()
                /*
                    RequestHandlerSelectors: 配置要扫描接口的方式
                    basePackage:指定要扫描的包 ---  (最常用的)
                    any()：扫描全部
                    none()：不做扫描
                    withClassAnnotation: 根据类上的注解进行扫描，参时是一个注解的反射对象
                    withMethodAnnotation: 根据方法上的数据扫描
                 */
                .apis(RequestHandlerSelectors.basePackage("nuc.edu.controller"))
                /*
                    paths()：过滤路径
                    PathSelectors：配置要扫描的路径
                    ant(): 过滤路径的格式
                        比如下面的/user/**：表示扫描访问路径以/user/开头的接口
                 */
                // .paths(PathSelectors.ant("/user/**"))
                .build();
    }

    public ApiInfo apiInfo(){
        Contact contact = new Contact("王韬", "https://www.baidu.com","2248964887qq.com");

        return new ApiInfo(
                "Swagger学习"     // 标题
                ,"演示Swagger"    // 描述
                ,"V1.0"     // 版本
                ,"https://www.baidu.com"    // 组织连接
                ,contact        // 联系人信息
                ,"Apach 2.0 许可"     // 许可
                ,"https://www.baidu.com"    //许可连接
                ,new ArrayList<>()  //扩展
        );
    }

}
