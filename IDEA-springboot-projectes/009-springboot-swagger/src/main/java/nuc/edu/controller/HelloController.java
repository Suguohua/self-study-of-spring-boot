package nuc.edu.controller;

import nuc.edu.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping(value = "/hello")
    public String hello() {
        return "hello, world!";
    }


    // 只要接口中，返回值存在实体类，就会被扫描在Swagger中，并显示在model位置上
    @GetMapping(value = "/getUser")
    public User getUser() {
        User user = new User(1,"zhangsan","123");
        return user;
    }

}
