package service.impl;

import model.User;
import service.UserService;

public class UserServiceImpl implements UserService {
    @Override
    public User queryUserById(Integer id) {
        User user = new User();
        user.setId(id);
        user.setUsername("张三");
        return user;
    }

    @Override
    public int queryAllUserCount() {
        return 100;
    }
}
