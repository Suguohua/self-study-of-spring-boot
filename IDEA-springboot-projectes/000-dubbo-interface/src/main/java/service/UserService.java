package service;

import model.User;

public interface UserService {

    /**
     * 根据用户标识回去用户信息
     * @param id
     * @return
     */
    User queryUserById(Integer id);

    /**
     * 获取所有用户的总人数
     * @return
     */
    int queryAllUserCount();

}
