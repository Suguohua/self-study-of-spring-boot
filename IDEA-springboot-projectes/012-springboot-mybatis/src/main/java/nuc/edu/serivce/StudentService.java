package nuc.edu.serivce;

import nuc.edu.model.Student;
import org.springframework.stereotype.Service;


public interface StudentService {

    /**
     * 根据id查询所有学生信息
     * @param id
     * @return
     */
    Student queryStudentById(Integer id);
}
