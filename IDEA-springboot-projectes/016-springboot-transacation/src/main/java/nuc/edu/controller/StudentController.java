package nuc.edu.controller;

import nuc.edu.service.StudentService;
import nuc.edu.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/update")
    public @ResponseBody Object update(Integer id, String name) {
        Student student = new Student();
        student.setId(id);
        student.setName(name);
        int result = studentService.updateStudentById(student);
        return "修改学生编号为:" + id + "的学生的姓名的结果为：" + result;
    }
}
