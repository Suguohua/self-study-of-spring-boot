package nuc.edu.service;

import nuc.edu.model.Student;

public interface StudentService {

    int updateStudentById(Student student);
}
