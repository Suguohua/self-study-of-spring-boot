package nuc.edu.service.impl;

import nuc.edu.mapper.StudentMapper;
import nuc.edu.model.Student;
import nuc.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Transactional()
    @Override
    public int updateStudentById(Student student) {
        int i = studentMapper.updateByPrimaryKeySelective(student);

        int a = 1/0;

        return i;
    }
}
