package nuc.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IndexController {

    @RequestMapping(value = "/say")
    @ResponseBody
    public String say() {
        return "say:SpringBoot!";
    }

    @RequestMapping("/map")
    @ResponseBody
    public Map<String , String> map() {
        Map<String, String> map = new HashMap<>();
        map.put("message", "hello StringBoot!");
        return map;
    }

}
