package nuc.edu.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {

    @RequestMapping("/user/json/detail")
    public @ResponseBody Object userJsonDetail() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1001);
        map.put("name", "zhangsan");
        return map;
    }

    @RequestMapping(value = "/user/page/Detail")
    public String userPage(Model model) {
        model.addAttribute("id", 1002);
        model.addAttribute("name", "lisi");
        return "userDetail";
    }

}
