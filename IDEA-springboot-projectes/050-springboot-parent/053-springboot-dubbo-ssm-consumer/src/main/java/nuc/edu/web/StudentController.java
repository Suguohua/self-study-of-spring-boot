package nuc.edu.web;

import com.alibaba.dubbo.config.annotation.Reference;
import nuc.edu.entity.Student;
import nuc.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
public class StudentController {

    @Reference(interfaceClass = StudentService.class, version = "1.0.0", check = false)
    private StudentService studentService;

    @RequestMapping("/student/detail/{id}")
    public String studentDetail(@PathVariable("id") Integer id, Model model) {

        Student student = studentService.queryStudentById(id);

        model.addAttribute("student", student);

        return "studentDetail";
    }
}
