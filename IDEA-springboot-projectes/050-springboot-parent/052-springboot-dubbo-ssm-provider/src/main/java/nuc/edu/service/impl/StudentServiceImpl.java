package nuc.edu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import nuc.edu.dao.StudentMapper;
import nuc.edu.entity.Student;
import nuc.edu.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Service(interfaceClass = StudentService.class, version = "1.0.0", timeout = 10000)
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Student queryStudentById(Integer id) {
        return studentMapper.selectByPrimaryKey(id);
    }
}
