package nuc.edu.service;

import nuc.edu.entity.Student;

public interface StudentService {
    Student queryStudentById(Integer id);
}
