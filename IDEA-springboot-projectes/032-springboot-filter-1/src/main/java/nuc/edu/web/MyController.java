package nuc.edu.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MyController {

    @RequestMapping("/user/filter")
    public @ResponseBody String filter() {
        return "aaa";
    }

    @RequestMapping("/filter")
    public @ResponseBody String filter1() {
        return "bbb";
    }
}
