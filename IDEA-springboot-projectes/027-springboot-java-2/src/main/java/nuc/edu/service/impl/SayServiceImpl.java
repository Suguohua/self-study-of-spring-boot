package nuc.edu.service.impl;

import nuc.edu.service.SayService;
import org.springframework.stereotype.Service;

@Service
public class SayServiceImpl implements SayService {
    @Override
    public String Say(String msg) {
        return "Say: " + msg;
    }
}
