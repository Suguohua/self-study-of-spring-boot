package nuc.edu;

import nuc.edu.service.SayService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		/*
		* SpringBoot程序启动后，返回值是一个ConfigurableApplicationContext，他也是一个SpringBoot容器
		* 它相当于原来的Spring容器中的启动容器ClasspathXmlApplicationContext
		* */

		ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);

		SayService sayService = (SayService) applicationContext.getBean("sayServiceImpl");
		String hallo = sayService.SayHallo();
		System.out.println(hallo);

	}

}
