package nuc.edu.web;

import nuc.edu.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class StudentController {

    @GetMapping(value = "/student")
    public Object student(Integer id, Integer age) {
        Student student = new Student();
        student.setId(id);
        student.setAge(age);
        return student;
    }


    //@RequestMapping(value = "/student/detail/{id}/{age}")
    @GetMapping(value = "student/detail/{id}/{age}")
    public Object student1(@PathVariable("id") Integer id, @PathVariable("age") Integer age) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("age", age);
        return map;
    }

    @DeleteMapping(value = "student/detail/{id}/{status}")
    public Object student2(@PathVariable("id") Integer id, @PathVariable("status") Integer status) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("status", status);
        return map;
    }


    @DeleteMapping(value = "student/{id}/detail/{city}")
    public Object student3(@PathVariable("id") Integer id, @PathVariable("city") Integer city) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("city", city);
        return map;
    }

    /*
    * 通常在RESTful风格中方法的请求方式会按照增删改查的请求方式来区分
    * RESTful请求风格要求在路径中使用的单词是名词，最好不要出现动词
    * */


}
